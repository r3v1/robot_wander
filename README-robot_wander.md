# Robótica Inteligente -- Master ICSI/KISA 2020-2021
## Práctica 1: Evitador de obstáculos

Primera práctica de la asignatura de Robótica Inteligente.

## Instalación

Tenemos que incluir el repo en `~/icsi/ros/src` para que la configuración del CMakeLists que haya en local lo lea bien.

```
cd ~/icsi/ros/src
git clone git@gitlab.com:r3v1/robot_wander.git
```

## Descripción

El propósito de este proyecto es el de evitar obstáculos leyendo datos
con el laser y ajustando la velocidad angular y la velocida lineal según
corresponda para evitar choques y facilitar el movimiento continuo.

## Autores

David - `drevillas002@ikasle.ehu.eus`

Mónica

